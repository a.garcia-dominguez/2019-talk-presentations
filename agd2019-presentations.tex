\documentclass[10pt,xcolor={table,svgnames}]{beamer}
\hypersetup{colorlinks,linkcolor=,urlcolor=orange}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\graphicspath{{}{figures/}}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{appendixnumberbeamer}

\usepackage{booktabs}
\usepackage[scale=2]{ccicons}
\usepackage{colortbl}

\usepackage{pgfplotstable,pgfplots}
\pgfplotsset{compat=1.12}
\usepgfplotslibrary{dateplot,colorbrewer}
\usetikzlibrary{patterns}

\usetikzlibrary{calc,fit,patterns}
\usepackage[absolute,overlay]{textpos}

\usepackage{xspace}
\newcommand{\themename}{\textbf{\textsc{metropolis}}\xspace}

\usepackage{amssymb}

\usepackage{xcolor}
\usepackage{listings}
\lstset{columns=fullflexible}

\lstdefinelanguage{dot}
{morekeywords={digraph,subgraph,node},
sensitive=false,
morecomment=[l]{//},
morecomment=[s]{/*}{*/},
morestring=[b]",
}

\input{preamble-drawings}

% "page cs" coordinate system
% From http://tex.stackexchange.com/questions/89588/
%
% Defining a new coordinate system for the page:
%
% --------------------------
% |(-1,1)    (0,1)    (1,1)|
% |                        |
% |(-1,0)    (0,0)    (1,0)|
% |                        |
% |(-1,-1)   (0,-1)  (1,-1)|
% --------------------------
\makeatletter
\def\parsecomma#1,#2\endparsecomma{\def\page@x{#1}\def\page@y{#2}}
\tikzdeclarecoordinatesystem{page}{
    \parsecomma#1\endparsecomma
    \pgfpointanchor{current page}{north east}
    % Save the upper right corner
    \pgf@xc=\pgf@x%
    \pgf@yc=\pgf@y%
    % save the lower left corner
    \pgfpointanchor{current page}{south west}
    \pgf@xb=\pgf@x%
    \pgf@yb=\pgf@y%
    % Transform to the correct placement
    \pgfmathparse{(\pgf@xc-\pgf@xb)/2.*\page@x+(\pgf@xc+\pgf@xb)/2.}
    \expandafter\pgf@x\expandafter=\pgfmathresult pt
    \pgfmathparse{(\pgf@yc-\pgf@yb)/2.*\page@y+(\pgf@yc+\pgf@yb)/2.}
    \expandafter\pgf@y\expandafter=\pgfmathresult pt
}
\makeatother
% Draws a grid for easier referencing of page cs values
\newcommand{\printtikzpagegrid}{
  \tiny
  \begin{tikzpicture}[overlay,remember picture,every node/.style={inner sep=.1em,draw=black!20,fill=white}]
    \foreach \x in {0,...,9} {
      \foreach \y in {0,...,9} {
        \node at (page cs:0.\x,0.\y) {};
        \node at (page cs:0.\x,-0.\y) {};
        \node at (page cs:-0.\x,0.\y) {};
        \node at (page cs:-0.\x,-0.\y) {};
      }
    }
    \node at (page cs:0,0) {0,0};
    \node at (page cs:0.5,0.5) {.5,.5};
    \node at (page cs:0.5,-0.5) {.5,-.5};
    \node at (page cs:-0.5,0.5) {-.5,.5};
    \node at (page cs:-0.5,-0.5) {-.5,-.5};
    \node at (page cs:1,1) {1,1};
    \node at (page cs:1,-1) {1,-1};
    \node at (page cs:-1,1) {-1,1};
    \node at (page cs:-1,-1) {-1,-1};
  \end{tikzpicture}
  \normalsize
}

\usepackage[space]{grffile}
\usepackage{textcomp}

\title{Tips and resources for publication-grade figures and tables}
%\subtitle{A modern beamer theme}
\date{20 June 2019}
\author{Antonio García-Domínguez}
%\institute{Center for modern beamer themes}
% \titlegraphic{\hfill\includegraphics[height=1.5cm]{logo.pdf}}

\newcommand{\package}[1]{\texttt{#1}}

\begin{document}

\maketitle

\begin{frame}{Table of contents}
  \setbeamertemplate{section in toc}[sections numbered]
  \tableofcontents[hideallsubsections]
\end{frame}

\section{Intro}

\begin{frame}{Why care about presentation quality?}

  \begin{block}{Readers are busy!}
    \begin{itemize}
    \item Readers and reviewers are busy academics
    \item They want to get the gist of your paper quickly
    \item Make their job easier with good graphics
    \item A good figure/table speaks a thousand words :-)
    \end{itemize}
  \end{block}

  \begin{block}{Competing with others}
    \begin{itemize}
    \item In some scenarios, you are competing for attention
    \item For instance, poster sessions in conferences
    \item Something eye-catchy and well presented is best here
    \end{itemize}
  \end{block}

  \begin{block}{Personal pride and satisfaction}
    \vspace{.5em}It's just nice when you can sum up your research in a pretty picture!
  \end{block}

\end{frame}

\begin{frame}{Disclaimer: \LaTeX{} ahead!}

  \begin{itemize}
  \item Many of the tips below will be using \LaTeX{} packages
  \item You can probably adapt the tips if not using \LaTeX
  \item Still, \LaTeX{} produces much better looks than Word :-)
    \pause
  \item ... seriously, the layouting algorithm is much better
  \end{itemize}

\end{frame}

\section{Basics}

\begin{frame}{Vector versus bitmap graphics}
  \framesubtitle{Unless you have to, use vector graphics}

  \begin{columns}
    \column{.5\textwidth}
    \begin{figure}
      \centering
      \includegraphics[height=.6\textheight,width=\columnwidth,keepaspectratio]{VectorBitmapExample}
      \caption{Vector versus bitmap graphics (Wikipedia)}
    \end{figure}

    \column{.5\textwidth}
    \begin{block}{Differences}
      \begin{itemize}
      \item Bitmap: array of pixels
      \item Vector: drawing instructions
      \end{itemize}
    \end{block}

    \begin{block}{Tradeoffs}
      \begin{itemize}
      \item Vector graphics can be scaled arbitrarily, and stay sharp
      \item Bitmap graphics cannot be scaled without pixelation
      \end{itemize}
    \end{block}

    \begin{block}{Common vector formats}
      \begin{itemize}
      \item SVG: easily editable, standard
      \item PDF: easily viewable, portable, and embeddable from \LaTeX{}
      \end{itemize}
    \end{block}
  \end{columns}

  % Orc image: zoom into vector vs raster at 100dpi
\end{frame}

\begin{frame}{Lossy versus lossless raster graphics}
  % Orc image: zoom into JPG vs PNG forms

  \begin{columns}
    \column{.5\textwidth}

    \begin{figure}
      \centering
      \includegraphics[width=\columnwidth,height=.6\textheight,keepaspectratio]{svg-quality-decrease}
      \caption{JPG image of a cat, with decreasing compression from left to right (and increasing image quality)}
    \end{figure}

    \column{.5\textwidth}
    \begin{block}{Lossless vs lossy compression}
      \begin{itemize}
      \item Lossless: bitwise-exact original
      \item Lossy: ``similar'' version
      \end{itemize}
    \end{block}

    \begin{block}{Example formats}
      \begin{itemize}
      \item Audio: FLAC vs OGG/MP3
      \item Images: PNG/GIF vs JPG
      \item H.264/H.265 supports both
      \end{itemize}
    \end{block}

    \begin{block}{When to use which?}
      \begin{itemize}
      \item Lossless for line drawings
      \item Lossy for photos at the last stage: preprocessing should be done
        using lossless originals
      \end{itemize}
    \end{block}

  \end{columns}

\end{frame}

\begin{frame}{Choosing a color schema: the color wheel}
  % Color wheel theory

  \begin{columns}
    \column{.5\textwidth}

    \begin{figure}
      \centering
      \includegraphics[width=\columnwidth,height=.75\textheight,keepaspectratio]{020_schiffermueller1.jpg}
      \caption{Ignaz Schiffermüller, Versuch eines Farbensystems (Vienna, 1772)}
    \end{figure}

    \column{.5\textwidth}
    \begin{block}{Structure}
      \begin{itemize}
      \item 3 primary colors (RYB / CMY)
      \item 3 secondary colors
      \item 6 tertiary colors
      \item + all colors between them
      \end{itemize}
    \end{block}

    \begin{block}{Schemes}
      \begin{itemize}
      \item Monochromatic: shades/tints
      \item Complementary: opposite
      \item Analogous: around center
      \item Triadic: triangle
      \item Tetradic: square
      \end{itemize}
    \end{block}

    Let's play around with
    \href{https://www.canva.com/colors/color-wheel/}{canva.com}. Uses for each?
  \end{columns}

\end{frame}

\begin{frame}{Choosing a color schema: ColorBrewer}

  \includegraphics[width=\textwidth]{colorbrewer}

  There are predesigned palettes for specific intents, considering
  colorblindness, photocopying and printability. Let's check
  \href{http://colorbrewer2.org}{ColorBrewer}.

  % Types of scales

  % Color blindness?

  % Print friendly?

  % B/W copy friendly?

  % What to do when you have too many colors? (e.g. using line styles, fill
  % patterns)
\end{frame}

\pgfplotstableread[col sep=comma]{data/20190127-times.csv}\csvtimes
\begin{frame}{Choosing a color schema: too many colors?}
  \input{figures/times-seams.tikz}

  \begin{itemize}
  \item Can't find colorblind-safe, print-friendly, B/W-friendly diverging
  schemas with >4 colors?
  \item Use pattern fills and line styles!
  \item Above example is from our SISSY 2019 paper :-)
  \end{itemize}

\end{frame}

\begin{frame}{Font families}
  % When to use each?

  \begin{block}{Examples}
    \begin{itemize}
    \item Serif: {\rmfamily The quick brown fox jumps over a lazy dog}
    \item Sans-serif: {\sffamily The quick brown fox jumps over a lazy dog}
    \item Monospaced: {\ttfamily The quick brown fox jumps over a lazy dog}
    \end{itemize}

    % By the way, this is the shortest \emph{pangram} in English (uses all 26
    % letters).
  \end{block}

  \begin{block}{Which one to use?}
    \begin{itemize}
    \item Generally, sans-serif on screen media, serif on print media
    \item Serifs help readability if rendered well (hence their use in books!)
    \item However, many screens lack the resolution to represent them well and
      distort their shape, cancelling their benefits
    \item This is especially bad with small font sizes!
      \begin{itemize}
      \item {\tiny The quick brown fox jumps over a lazy dog}
      \item {\tiny\rmfamily The quick brown fox jumps over a lazy dog}
      \end{itemize}
    \end{itemize}
  \end{block}

\end{frame}

\section{Tables}

\newcommand{\cellyesA}{\cellcolor{green!20}yes}
\newcommand{\cellnoA}{\cellcolor{red!20}no}

\begin{frame}{Starting point: a Word-style tableau}
  % Start with a table with all rules, everything centered, data repeated,
  % colors all over the place, mismatched decimal places.

  \begin{table}
    \large 
    \rowcolors{2}{gray!25}{white}
    \begin{tabular}{|c|c|c|c|c|c|}
      \hline
      \rowcolor{black}\color{white}ColA & \color{white}ColB & \color{white}ColC & \color{white}ColD & \color{white}ColE \\
      \hline
      A1 & B1 & Red & 123.4 & \cellyesA \\ \hline
      A1 & B1 & Green & 12 & \cellnoA \\ \hline
      A1 & B2 & Red & 274.37 & \cellnoA \\ \hline
      A1 & B2 & Green & 92.1 & \cellyesA \\ \hline
      A2 & B1 & Red & 108.7 & \cellnoA \\ \hline
      A2 & B1 & Green & 121.840 & \cellyesA \\ \hline
      A2 & B2 & Red & 12.34 & \cellnoA \\ \hline
      A2 & B2 & Green & 45.678 & \cellyesA \\ \hline
    \end{tabular}
  \end{table}

  \begin{center}
    This is what a typical ``table'' in Word could look like.

    We used lots of unnecessary formatting here: let's clean up.
  \end{center}

\end{frame}

\begin{frame}{First step: align well}
  % Numbers are right-aligned, text is left-aligned

  \begin{table}
    \large 
    \rowcolors{2}{gray!25}{white}
    \begin{tabular}{|l|l|l|r|l|}
      \hline
      \rowcolor{black}\multicolumn{1}{c}{\color{white}ColA} & \multicolumn{1}{c}{\color{white}ColB} & \multicolumn{1}{c}{\color{white}ColC} & \multicolumn{1}{c}{\color{white}ColD} & \multicolumn{1}{c}{\color{white}ColE} \\
      \hline
      A1 & B1 & Red & 123.4 & \cellyesA \\ \hline
      A1 & B1 & Green & 12 & \cellnoA \\ \hline
      A1 & B2 & Red & 274.37 & \cellnoA \\ \hline
      A1 & B2 & Green & 92.1 & \cellyesA \\ \hline
      A2 & B1 & Red & 108.7 & \cellnoA \\ \hline
      A2 & B1 & Green & 121.840 & \cellyesA \\ \hline
      A2 & B2 & Red & 12.34 & \cellnoA \\ \hline
      A2 & B2 & Green & 45.678 & \cellyesA \\ \hline
    \end{tabular}
  \end{table}

  \begin{center}
    In left-to-right cultures, text is left-centered, and numbers are
    right-centered for easy addition / comparison.

    However, the numbers still feel off.
  \end{center}

\end{frame}

\begin{frame}{Second step: really align well}
  % Make decimal places match and align!

  % Numbers are right-aligned, text is left-aligned

  \begin{table}
    \large 
    \rowcolors{2}{gray!25}{white}
    \begin{tabular}{|l|l|l|r|l|}
      \hline
      \rowcolor{black}\multicolumn{1}{c}{\color{white}ColA} & \multicolumn{1}{c}{\color{white}ColB} & \multicolumn{1}{c}{\color{white}ColC} & \multicolumn{1}{c}{\color{white}ColD} & \multicolumn{1}{c}{\color{white}ColE} \\
      \hline
      A1 & B1 & Red & 123.40 & \cellyesA \\ \hline
      A1 & B1 & Green & 12.00 & \cellnoA \\ \hline
      A1 & B2 & Red & 274.37 & \cellnoA \\ \hline
      A1 & B2 & Green & 92.10 & \cellyesA \\ \hline
      A2 & B1 & Red & 108.70 & \cellnoA \\ \hline
      A2 & B1 & Green & 121.84 & \cellyesA \\ \hline
      A2 & B2 & Red & 12.34 & \cellnoA \\ \hline
      A2 & B2 & Green & 45.68 & \cellyesA \\ \hline
    \end{tabular}
  \end{table}

  \begin{center}
    We didn't have a consistent number of decimal places in our numbers.

    Now we can easily compare numbers at a glance, and order-of-magnitude
    differences are clear through whitespace.
  \end{center}

\end{frame}

\begin{frame}{Third step: drop vertical rules}

  \begin{table}
    \large 
    \rowcolors{2}{gray!25}{white}
    \begin{tabular}{lllrl}
      \hline
      \rowcolor{black}\multicolumn{1}{c}{\color{white}ColA} & \multicolumn{1}{c}{\color{white}ColB} & \multicolumn{1}{c}{\color{white}ColC} & \multicolumn{1}{c}{\color{white}ColD} & \multicolumn{1}{c}{\color{white}ColE} \\
      \hline
      A1 & B1 & Red & 123.40 & \cellyesA \\ \hline
      A1 & B1 & Green & 12.00 & \cellnoA \\ \hline
      A1 & B2 & Red & 274.37 & \cellnoA \\ \hline
      A1 & B2 & Green & 92.10 & \cellyesA \\ \hline
      A2 & B1 & Red & 108.70 & \cellnoA \\ \hline
      A2 & B1 & Green & 121.84 & \cellyesA \\ \hline
      A2 & B2 & Red & 12.34 & \cellnoA \\ \hline
      A2 & B2 & Green & 45.68 & \cellyesA \\ \hline
    \end{tabular}
  \end{table}

  \begin{center}
    Vertical rules add a lot of noise --- we do not need them!
  \end{center}

\end{frame}

\begin{frame}{Fourth step: drop colour}

  \begin{table}
    \large 
    \begin{tabular}{lllrl}
      \hline
      \multicolumn{1}{c}{ColA} & \multicolumn{1}{c}{ColB} & \multicolumn{1}{c}{ColC} & \multicolumn{1}{c}{ColD} & \multicolumn{1}{c}{ColE} \\
      \hline
      A1 & B1 & Red & 123.40 & yes \\ \hline
      A1 & B1 & Green & 12.00 & no \\ \hline
      A1 & B2 & Red & 274.37 & no \\ \hline
      A1 & B2 & Green & 92.10 & yes \\ \hline
      A2 & B1 & Red & 108.70 & no \\ \hline
      A2 & B1 & Green & 121.84 & yes \\ \hline
      A2 & B2 & Red & 12.34 & no \\ \hline
      A2 & B2 & Green & 45.68 & yes \\ \hline
    \end{tabular}
  \end{table}

  \begin{center}
    Colour creates noise as well --- drop it for now.

    We will add back that structure in a moment.
  \end{center}

\end{frame}

\begin{frame}{Fifth step: use line weights, spacing and font weight}

  \begin{table}
    \large 
    \begin{tabular}{lllrl}
      \toprule
      \multicolumn{1}{c}{\textbf{ColA}} & \multicolumn{1}{c}{\textbf{ColB}} & \multicolumn{1}{c}{\textbf{ColC}} & \multicolumn{1}{c}{\textbf{ColD}} & \multicolumn{1}{c}{\textbf{ColE}} \\
      \midrule
      A1 & B1 & Red & 123.40 & yes \\ \midrule
      A1 & B1 & Green & 12.00 & no \\ \midrule
      A1 & B2 & Red & 274.37 & no \\ \midrule
      A1 & B2 & Green & 92.10 & yes \\ \midrule
      A2 & B1 & Red & 108.70 & no \\ \midrule
      A2 & B1 & Green & 121.84 & yes \\ \midrule
      A2 & B2 & Red & 12.34 & no \\ \midrule
      A2 & B2 & Green & 45.68 & yes \\
      \bottomrule
    \end{tabular}
  \end{table}

  \begin{center}
    Let's switch to \package{booktabs} and use top/mid/bottom rules.
  \end{center}

\end{frame}

\begin{frame}{Sixth step: avoid repetition and embrace the emptiness!}

  \begin{table}
    \large 
    \begin{tabular}{lllrc}
      \toprule
      \multicolumn{1}{c}{\textbf{ColA}} & \multicolumn{1}{c}{\textbf{ColB}} & \multicolumn{1}{c}{\textbf{ColC}} & \multicolumn{1}{c}{\textbf{ColD}} & \textbf{ColE} \\
      \midrule
      A1 & B1 & Red   & 123.40 & \checkmark \\ \midrule
         &    & Green & 12.00  &            \\ \midrule
         & B2 & Red   & 274.37 &            \\ \midrule
         &    & Green & 92.10  & \checkmark \\ \midrule
      A2 & B1 & Red   & 108.70 &            \\ \midrule
         &    & Green & 121.84 & \checkmark \\ \midrule
         & B2 & Red   & 12.34  &            \\ \midrule
         &    & Green & 45.68  & \checkmark \\
      \bottomrule
    \end{tabular}
  \end{table}

  \begin{center}
    We have removed all values that are repeated across rows, and left ``no'' as
    implicit value for empty spaces in ColE.
  \end{center}

\end{frame}

\begin{frame}{Final step: adjust lengths of midrules}

  \begin{table}
    \large 
    \begin{tabular}{lllrc}
      \toprule
      \multicolumn{1}{c}{\textbf{ColA}} & \multicolumn{1}{c}{\textbf{ColB}} & \multicolumn{1}{c}{\textbf{ColC}} & \multicolumn{1}{c}{\textbf{ColD}} & \textbf{ColE} \\
      \midrule
      A1 & B1 & Red   & 123.40 & \checkmark \\ \cmidrule{3-5}
         &    & Green & 12.00  &            \\ \cmidrule{2-5}
         & B2 & Red   & 274.37 &            \\ \cmidrule{3-5}
         &    & Green & 92.10  & \checkmark \\ \midrule
      A2 & B1 & Red   & 108.70 &            \\ \cmidrule{3-5}
         &    & Green & 121.84 & \checkmark \\ \cmidrule{2-5}
         & B2 & Red   & 12.34  &            \\ \cmidrule{3-5}
         &    & Green & 45.68  & \checkmark \\
      \bottomrule
    \end{tabular}
  \end{table}

  \begin{center}
    We tweak the length of the various midrules to create visual structure.
  \end{center}

\end{frame}


\begin{frame}{Thoughts so far}

  \begin{block}{Word is not a good guide for good formatting}
    \begin{itemize}
    \item People got used to formatting like Word suggested
    \item Word was not made by professional typesetters, though...
    \end{itemize}
  \end{block}

  \begin{block}{Recommendation: the \package{booktabs} package}
    \begin{itemize}
    \item Easy to use, produces better results
    \item Documentation sums up basic rules for good tables, and compares proper
      tables against the \emph{tableaux} people usually go for
    \item In general:
      \begin{itemize}
      \item Avoid repetition or \emph{ditto} signs
      \item Take advantage of visual grouping through whitespace
      \item Avoid useless ``noise'' in your tables and figures
      \end{itemize}
    \end{itemize}
  \end{block}

\end{frame}

\begin{frame}[fragile]{Data-driven tables: \package{pgfplotstable}}

  \begin{columns}
    \column{.35\textwidth}
    \begin{table}
      \centering
      \large
      \pgfplotstabletypesetfile[
        every head row/.style={
          before row={\toprule},
          after row={\midrule},
        },
        every last row/.style={
          after row={\bottomrule}
        },
        columns/a/.style={
          column name={\textbf{A}},
          int detect,
        },
        columns/b/.style={
          column name={\textbf{B}},
          sci,sci zerofill,precision=2
        }
      ]{data/example.csv}
      \caption{Output from snippet on the right}
    \end{table}

    \column{.65\textwidth}
    \begin{lstlisting}[language=tex,numbers=right,numberstyle=\scriptsize,numbersep=-20pt]
\pgfplotstabletypesetfile[
  every head row/.style={
    before row={\toprule},
    after row={\midrule}},
  every last row/.style={
    after row={\bottomrule}},
  columns/a/.style={
    column name={\textbf{A}},
    int detect},
  columns/b/.style={
    column name={\textbf{B}},
    sci,sci zerofill,precision=2}
]{data/example.csv}
    \end{lstlisting}
  \end{columns}

  \begin{center}
    Keeps your data separate from your formatting.

    R can produce \LaTeX{} tables as well, but this is usually easier!
  \end{center}

\end{frame}

\section{Charts}

\begin{frame}{What do you mean with a chart?}

  \begin{itemize}
  \item You represent objects in a 2D space
  \item You relate them together with arrows/lines
  \item You group them with containers
  \item Essentially, ``boxes and arrows''
  \end{itemize}

  We will discuss numerical plots later!

\end{frame}

\begin{frame}{Inkscape: general vector drawing tool}

  \begin{center}
    \includegraphics[width=.9\textwidth]{inkscape-geram}
  \end{center}

  \begin{itemize}
  \item Open-source, free to use, standards-based (SVG)
  \item Great for custom figures or to finish generated SVGs
  \item Takes a while to master, though
  \item That one is from my PhD dissertation :-)
  \end{itemize}

\end{frame}

\begin{frame}{yEd: graph-oriented graphics editor}

  \begin{center}
    \includegraphics[width=.9\textwidth]{yed}
  \end{center}

  \begin{itemize}
  \item Closed-source, but still free to use
  \item Understands graphs (nodes + edges)
  \item Provides automated layouting
  \item Supports a few standard notations (flowcharts, UML, BPMN, E/R)
  \end{itemize}

\end{frame}

\begin{frame}[fragile]{Graphviz: DSL for autolayouted graphs}

  \begin{columns}
    \column{.5\textwidth}
    \begin{figure}
      \includegraphics[width=\columnwidth]{info-flows}
    \end{figure}

    \column{.5\textwidth}
    \begin{lstlisting}[language=dot,basicstyle=\small,columns=flexible]
digraph InfoFlows {
 subgraph Tasks {
  node [shape=box, style=filled,
   bgcolor=lightgray];
    t1 [label="Task 1..."];
    ...
 }
 ...
 production -> t3 [label="Prod..."];
 ...
}
    \end{lstlisting}

    \begin{itemize}
    \item \package{dot}: hierarchical digraphs
    \item \package{neato}: energy-based springs
    \item \package{fdp}: force-based springs
    \item \package{sfdp}: multiscale (large graphs)
    \item \package{twopi}: radial
    \item \package{circo}: circular
    \end{itemize}

  \end{columns}

\end{frame}

\begin{frame}{TikZ: powerful LaTeX graphics package}

  % More capabilities: formulas, exact positioning, loops, autolayout (with
  % LuaTeX). Amazing gallery in texample.net - check a few examples.

  \begin{figure}
    \centering
    \input{samples/time-limits.tikz}
    \caption{Sample TikZ figure}
  \end{figure}

  \begin{itemize}
  \item TikZ: TikZ ist kein Zeichenprogramm (\LaTeX{} package)
  \item Huge set of macros and libraries for drawing diagrams
  \item Based on PGF (same core as \package{pgfplots}, shown later)
  \item Latest version supports autolayouts if using LuaTeX
  \end{itemize}

  \begin{center}
    Let's check \href{http://www.texample.net/tikz/examples/}{texamples.net}!
  \end{center}

\end{frame}

\section{Graphs}

\begin{frame}{Things to consider when plotting your data}

  \begin{block}{What is the data about?}
    \begin{itemize}
    \item Distribution over a space = points in space $\rightarrow$ scatter plot
    \item Evolution over time/size = line/area over axis $\rightarrow$ line plot
    \item Ratio over total = area comparison $\rightarrow$ stacked bar/area plot
    \item Comparisons between alternatives = shape comparison $\rightarrow$ radar plot
    \end{itemize}
  \end{block}

  \begin{block}{What is this plot for?}
    \begin{itemize}
    \item Print media (poster, paper)?
    \item Non-interactive screen media (slides)?
    \item Interactive screen media (web)?
    \end{itemize}
  \end{block}

\end{frame}

\begin{frame}{By the way, let's kill the (3D) pie chart}
  \begin{figure}
    \centering
    \includegraphics[width=.8\textwidth]{pie-chart-3d}
    \caption{3D pie charts, as awful as ever}
  \end{figure}

  \begin{center}
    Can you guess what are the percentages?

    The fake 3D adds absolutely nothing here.
  \end{center}
\end{frame}

\begin{frame}{By the way, let's kill the (3D) pie chart}
  \begin{figure}
    \centering
    \includegraphics[width=.8\textwidth]{pie-chart-2d}
    \caption{2D pie charts, slightly less useless}
  \end{figure}

  \begin{center}
    If you have a good eye for pizza, you should be able to guess now.
  \end{center}
\end{frame}

\begin{frame}{By the way, let's kill the (3D) pie chart}
  \begin{figure}
    \centering
    \includegraphics[width=.8\textwidth]{stacked-bar-chart}
    \caption{Stacked bar charts are always a better option}
  \end{figure}

  \begin{center}
    It's much easier to compare areas of rectangles than pie slices.

    We also get a very nice X axis that readers can refer to.
  \end{center}
\end{frame}

\begin{frame}{By the way, let's kill the (3D) pie chart}
  \begin{figure}
    \centering
    \includegraphics[width=.8\textwidth]{stacked-bar-chart-numbers}
    \caption{Now with the answers}
  \end{figure}

  \begin{center}
    How did you do?

    In general, think of the reader when formatting!
  \end{center}
\end{frame}

\begin{frame}{Simple LaTeX data plots: pgfplots}

  \begin{figure}
    \input{figures/pgfplots.tikz}
    \caption{X-Y plot with \package{pgfplots}}
  \end{figure}

  \begin{itemize}
  \item Provides both 2D and 3D plots: line/area/bars/mesh/quiver plots
  \item Has its own 500-page reference + manual
  \item Built on top of the same PGF core used by TikZ
  \item Main advantage: can easily combine with high-quality math formulas,
    typesetting and fonts from \LaTeX
  \item Your slides and your figures will look consistent and professional
  \end{itemize}

  % Great for XY plots in general, including stacking/grouping

  % Can generate good plots straight from LaTeX, exact text as main paper
\end{frame}

\begin{frame}{Advanced data plots: R}

  \begin{columns}
    \column{.5\textwidth}
    \begin{figure}
      \centering
      \resizebox{!}{.8\textheight}{\input{figures/rq3-comparison-tb}}

      Query execution times with various configurations of Hawk (ms)
    \end{figure}

    \column{.5\textwidth}

    \begin{itemize}
    \item R is \textbf{the} data science language
    \item \textbf{Very} quirky, but powerful
    \item Many non-standard viz available
    \item \package{ggplot2} library is very flexible
    \item Let's check the \href{https://www.r-graph-gallery.com/}{R Graph Gallery}
    \end{itemize}

    \begin{block}{R + TikZ}
      \begin{itemize}
      \item \package{tikzDevice} library generates TikZ code from your plot
      \item You can combine R data analysis with \LaTeX{} typesetting
      \item Heard about this one from Felipe yesterday! :-)
      \end{itemize}
    \end{block}

  \end{columns}

  % R has some amazing packages

  % ggplot2 is there, but there are many others - check the
  % https://www.r-graph-gallery.com/ gallery!

  % I can show off our radar chart and explain how it was useful
\end{frame}

\begin{frame}{Interactive data plots for the web: D3 and C3.js (I)}

  \begin{figure}
    \includegraphics[width=\textwidth]{d3}
  \end{figure}

  \begin{itemize}
  \item D3 is a JavaScript library which can generate/bind SVG to data
  \item Basically, you can make *any* drawing data-aware, duplicating elements
    and changing location/size/appearance based on the data
  \item Since it runs in a browser, it can be interactive and animated
  \item Let's check the \href{https://www.d3-graph-gallery.com/}{D3 Graph Gallery} and the \href{https://d3js.org/}{official website}
  \end{itemize}

\end{frame}

\begin{frame}{Interactive data plots for the web: D3 and C3.js (II)}

  \begin{figure}
    \includegraphics[width=\textwidth]{c3}
  \end{figure}

  \begin{itemize}
  \item As powerful as D3 is, sometimes we just want a standard chart
  \item C3.js implements those on top of D3 in a much easier way
  \item C3.js provides some standard controls (focus, disable/enable...)
  \item We still get the benefits of smooth animations and interactivity
  \item We can check the \href{https://c3js.org/examples.html}{C3.js examples}, too
  \end{itemize}

\end{frame}

\appendix

\begin{frame}[standout]

  \begin{center}
    {\huge Thank you!}

    Resources are here:
    \url{https://bit.ly/2RnRATh}

    \IfFileExists{\jobname-copy.pdf}{%
      \begin{center}
        \foreach \i in {4,8,15,27,33,38} {
          \hyperlink{page.\i}{\includegraphics[width=0.3\linewidth, page=\i]{\jobname-copy.pdf}}
        }
      \end{center}
    }{\typeout{No Thumbnails included}}

    a.garcia-dominguez@aston.ac.uk

    @antoniogado
  \end{center}

\end{frame}

\end{document}
