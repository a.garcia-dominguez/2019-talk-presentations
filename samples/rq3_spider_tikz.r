#!/usr/bin/Rscript

library(tikzDevice)
library(fmsb)

# Read .csv file in this directory
getScriptPath <- function(){
  cmd.args <- commandArgs()
  m <- regexpr("(?<=^--file=).+", cmd.args, perl=TRUE)
  script.dir <- dirname(regmatches(cmd.args, m))
  if (length(script.dir) == 0)
    # The file was source()d instead of being run from Rscript
    dirname(sys.frame(1)$ofile)
  else if (length(script.dir) > 1)
    stop("can't determine script dir: more than one '--file' argument detected")
  else
    script.dir
}
script.dir <- getScriptPath()
pathCSV <- file.path(script.dir, 'railway32.csv')

dataCSV <- read.csv(
  file=pathCSV, sep = "\t",
  col.names=c('tool','lang','protocol', 'query', 'threads', 'millis', 'results'),
  na.strings = c('ERROR',  'NA'),
  colClasses = c('factor','factor','factor','factor','integer','integer','integer'))

# Compute medians by tool/protocol/query/threads
mediansLong <- aggregate(millis~tool+lang+protocol+query+threads, dataCSV, median)

# Leave only TCP entries + 1 thread + remove SemaphoreNeighbor from CDO
mediansLong <- mediansLong[mediansLong$protocol == 'tcp' & mediansLong$threads == 1 & (mediansLong$query != 'SemaphoreNeighbor' | mediansLong$tool != 'cdo-h2'),  ]
mediansLong$threads <- NULL

# Leave only desired tools
mediansLong <- mediansLong[with(mediansLong, tool == 'cdo-h2' | tool == 'hawk-neo4j' | tool == 'hawk-orient' | tool == 'mogwai4'), ]

# Place medians together
mediansWide <- reshape(mediansLong, timevar=c('query'), idvar=c('tool','lang','protocol'), direction='wide')
names(mediansWide) <- sapply(names(mediansWide), function (x) { stringi::stri_replace_first_fixed(x, 'millis.', '') })
names(mediansWide)[4:9] <- sapply(names(mediansWide)[4:9], function (x) { stringi::stri_replace_all_regex(x, '[a-z]', '') })
mediansWide <- mediansWide[order(mediansWide$tool, mediansWide$lang),]
mediansWide <- mediansWide[c(2,3,4,5,1,6), ]

# Add min/max rows
spiderData <- mediansWide[, 4:9]
minRow <- sapply(spiderData, function(x) { 1 })
maxRow <- sapply(spiderData, function(x) { max(x, na.rm=TRUE) })
combined <- rbind(maxRow, minRow, spiderData)

out.pdf <- file.path(script.dir, 'rq3-comparison-tb.pdf')
tikz(file='rq3-comparison-tb.tex', width=8, height=9, documentDeclaration='\\documentclass[10pt]{beamer}')
#cairo_pdf(out.pdf, width=16, height=9)
#layout(matrix(c(1, 2), 1, 2, byrow=TRUE))

colors <- c('black','red','brown','blue')
radarchart(
  combined[1:6,], pcol=colors, axistype=2,
  cglwd=1, cglty=1, cglcol='gray', pty=1:6, plwd=2, title='Hawk', cex.main=2,  vlcex=1.5, palcex=1.4,
  paxislabels=c(
    paste0(maxRow[1], '\n\n'),
    paste0(maxRow[2], '\\phantom{xxxxx}'),
    paste0(maxRow[3], '\\phantom{xxxxx}'),
    paste0('\n\n', maxRow[4]),
    paste0('\\phantom{xxxxx}', maxRow[5]),
    paste0('\\phantom{xxxxx}', maxRow[6])
  )
)
legend(.5, 1.3, legend = c('Neo4j, EOL', 'Neo4j, EPL', 'Orient, EOL', 'Orient, EPL'),
       col=colors, seg.len=2, border = "transparent", pch = 1:6, lty = 1:6, cex=1.4)

# radarchart(
#   combined[c(1,2,7,8), ], pcol=colors, axistype=2,
#   cglwd=1, cglty=1, cglcol='gray', pty=1:6, plwd=2, title='Mogwai/CDO', cex.main=2, vlcex=1.5, palcex=1.4,
#   paxislabels=c(
#     paste0(maxRow[1], '\n\n'),
#     paste0(maxRow[2], '\\phantom{xxxxx}'),
#     paste0(maxRow[3], '\\phantom{xxxxx}'),
#     '\n\nno data',
#     paste0('\\phantom{xxxxx}', maxRow[5]),
#     paste0('\\phantom{xxxxx}', maxRow[6])
#   )
# )
# legend(.5, 1.3, legend = c('CDO', 'Mogwai'),
#        col=colors, seg.len=2, border = "transparent", pch = 1:6, lty = 1:6, cex=1.4)

dev.off()