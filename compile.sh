#!/bin/bash

JOB=agd2019-presentations

set -x

latexmk -pdf "$JOB"
cp "$JOB"{.pdf,-copy.pdf}
latexmk -C "$JOB"
latexmk -pdf "$JOB"
