Tips and resources for publication-grade figures and tables
===

These are slides and resources for a 1h talk on resources for high-quality figures and tables in presentations. `samples` includes some samples from the mentioned technologies, from my past papers, my PhD dissertation, and various resources on the web.

Abstract
---
High-quality figures and tables can make your paper look more professional and improve your chances of being published.
The right presentation will help conveying your conclusions to your audience.
In this talk, I will mention some of the approaches I have used in the past.
This will include topics such as vector vs raster graphics, the design of publication-grade tables, some advanced packages for LaTeX, the advantages of graph-oriented drawing and layout tools, and the breadth of advanced visualisations available in R.

Short bio
---
Antonio is a Lecturer in Computer Science at Aston, interested in scalable object-oriented modelling of large complex systems, self-explanation in
self-adaptive models@run.time systems, and software testing.
Antonio is the main developer of the Eclipse Hawk open-source project, and one of the core developers of the Eclipse Epsilon project.
Antonio has participated in local, national and European research projects, and has recently won a 3-year Knowledge Transfer Partnership grant for work in profiling large unstructured document collections with FoldingSpace.
